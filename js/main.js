const api_key = '1977b733';
const api_uri = `http://www.omdbapi.com/?apikey=${api_key}`;
const api_data_length = 10; // how much items in search maximum could be

$(document).ready(function() {
    var $fader = $('#fader'),
        $faderbody = $('#faderbody'),
        $form = $('#form'),
        $list = $('#list'),
        $pagination = $('#pagination');

    initialize();

    function initialize() {
        $form.on('submit', function() {
            sendRequest(buildRequestUri())
                .then(json => buildList(json));
        });

        setupQuery();
    }

    // checks if uri have "api"-query and initialzes form fields with it
    function setupQuery() {
        var q = location.search.match(/api=([^\&]+)/);
        if (!q) {
            return;
        }

        // parse query from "api"-query
        var a = document.createElement('a');
        a.href = decodeURIComponent(q[1]);
        var query = a.search.match(/[^=\?\&]+=[^\&]+/g).reduce((props, query) => {
            var [name, value] = query.split('=');
            props[name] = value;
            return props;
        }, {});

        Object.values($form.get(0))
            .forEach(node => {
                var value = query[node.name];
                if (typeof value !== 'undefined') {
                    node.value = value;
                }
            });

        sendRequest(`${a.href}`)
            .then(json => buildList(json, +(query.page || 1) - 1));
    }

    // builds uri with query from all fields of form (w/o pagination)
    function buildRequestUri() {
        return Object.values($form.get(0))
            .filter(node => node.name && node.value)
            .reduce((uri, node) => `${uri}&${node.name}=${node.value}`, api_uri);
    }

    // sends request on api with specified url
    function sendRequest(uri) {
        // check on required request parameters
        if (!uri.match(/\&s=.+/)) {
            throw new Error(`Title is required, but ${uri} presented`);
        }

        changeHistory(uri);

        return getResponse(uri);
    }

    function getResponse(uri) {
        var fadeTimer = setTimeout(() => $fader.modal('show'), 1000);
        return fetch(uri)
            .then(r => r.json(), err => error(err))
            .then(json =>
                // if films count is very big then api returns empty response
                json.Response == 'True' ? json : Promise.reject(json.Error.match(/found/i) ? 'Movie not found' : 'Specify search')
            )
            .then(res => {
                clearTimeout(fadeTimer);
                $fader.modal('hide');
                return res;
            }, err => error(err));
    }

    // changes uri of browser and title
    function changeHistory(uri) {
        history.replaceState({},
            document.head.title,
            `${location.protocol}//${location.host}${location.pathname}?api=${encodeURIComponent(uri)}`
        );
    }

    function buildList(json, pageIndex) {
        fillList(json.Search);
        // count of pages
        var count = Math.ceil(json.totalResults / api_data_length);
        buildPagination(count, pageIndex);
    }

    // clears old list and appends new info
    function fillList(list) {
        $list.empty();

        // concatinate all dom data in one string
        $list.append(list.reduce((dom, item) => `${dom}
            <div class="col-md-4">
        		<div class="card mb-4 box-shadow">
            		<img class="card-img-top" src="${item.Poster}" data-holder-rendered="true">
            		<div class="card-body">
            		    <p class="card-text">${item.Title}</p>
            		    <div class="d-flex justify-content-between align-items-center">
            		        <div class="btn-group">
            		            <button type="button" class="btn btn-sm btn-outline-secondary details-btn" data-imdbID="${item.imdbID}">Details</button>
            		        </div>
            		        <small class="text-muted">${item.Year}</small>
            		    </div>
            		</div>
            	</div>
        	</div>`, ''));

        Array.prototype.forEach.call($list.find('.card-img-top'), node => {
            var img = new Image;
            img.src = node.src;
            img.onerror = () => node.src = 'https://dexx.com.cy/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/placeholder/default/2000px-No_image_available.svg.png';
        });

        $list.find('.details-btn').on('click', function() {
            getResponse(`${api_uri}&i=${this.getAttribute('data-imdbID')}`)
                .then(res => {
                    $(this).parents('.card-body')
                        .empty()
                        .append(Object.keys(res)
                            .filter(key => ['Response'].indexOf(key) == -1)
                            .reduce((dom, key) => `${dom}
                				<div class="list-group">
                            	    <a class="list-group-item">
                            	        <i class="fa fa-comment fa-fw"></i> ${key}
                            	        <span class="pull-right text-muted small"><em>${res[key]}</em>
                            	        </span>
                            	    </a>
                            	</div>`, ''));
                });
        });
    }

    // builds pagination menu, ex: (with index 0) 1, 2, 99 (99 - is last page index)
    function buildPagination(count, index = 0) {
        $pagination.empty();

        // array with pagination view: "1, 5, 6, 7, 99"
        var pagesIndexes = [1, ...(new Array(3)).fill(index).map((val, i) => val + i), count]
            .filter((val, i, arr) => val > 0 && val <= count && arr.lastIndexOf(val) <= i);

        $pagination.append(pagesIndexes.reduce((dom, i) => `${dom}
			<label class="btn btn-secondary${i == index + 1 ? ' active"' : '" data-value=' + i}>
			    <input type="radio" name="options">${i}
			</label>`, ''));

        $pagination.find('label').on('click', function() {
            var value = this.getAttribute('data-value');
            if (value) {
                sendRequest(`${buildRequestUri()}&page=${value}`)
                    .then(json => buildList(json, +value - 1));
            }
        });
    }

    function error(err) {
        $fader.modal('show');
        $faderbody.text(err);
    }
});